(function () {
  'use strict';

  angular
    .module('axier')
    .controller('AppController', AppController);

  function AppController($window, $rootScope, $timeout) {

    $rootScope.appConfigs = {
      "searchOpen": false
    };

    $timeout(function () {
      document.getElementsByClassName('page-loading-overlay')[0].classList.add('loaded')
    });

    angular.module('axier.app').requires.push('ngAnimate');

    var init = function () {
      var chart = document.getElementById('chart-1');
      chart.className += " chart-2";
      createProgressCircle(chart, '$');
    };

    function createProgressCircle(el, curr) {
      el.style.height = el.getAttribute('data-size') + 'px';
      el.style.width = el.getAttribute('data-size') + 'px';

      var options = {
        percent: el.getAttribute('data-percent') || 25,
        revenue: el.getAttribute('data-revenue') || 25,
        adr: el.getAttribute('data-adr') || 25,
        size: el.getAttribute('data-size') || 220,
        lineWidth: el.getAttribute('data-line') || 5,
        rotate: el.getAttribute('data-rotate') || 0,
        color: el.getAttribute('data-fill-color') || '#d35400',
        // channelName: el.getAttribute('data-channel-name') || null
      }

      var canvas = document.createElement('canvas');
      var span = document.createElement('span');
      span.style.height = options.size + 'px';
      span.style.width = options.size + 'px';
      span.style.lineHeight = (parseInt(options.size)) + 'px';
      span.style.fontSize = (options.size / 4) + 'px';
      span.className += "percentage_value";
      // span.innerHTML = helpersService.abbreviateInt(parseInt(options.revenue));
      span.innerHTML = '25';

      var sup = document.createElement('sup');
      sup.textContent = '%';
      sup.style.top = '-7px';
      sup.style.fontSize = '21px';
      span.appendChild(sup);
      // var btag = document.createElement('span');
      // btag.textContent = 'bookings';
      // var mtop = ((parseInt(options.size) * 18.1818) / 100);
      // btag.style.top = mtop + 'px';
      // btag.className += " btag";

      // var adr = document.createElement('span');
      // adr.textContent = options.adr;

      // var adrt = document.createElement('sup');
      // adrt.textContent = ' ADR';

      // var adrc = document.createElement('sup');
      // adrc.textContent = curr;
      // adrc.className += "adrc";

      // $(adr).prepend(adrc);
      // adr.appendChild(adrt);
      // var mtop2 = (-(parseInt(options.size) * 30) / 100);
      // adr.style.top = mtop2 + 'px';
      // adr.style.fontSize = (options.size / 6) + 'px';
      // adr.className += " adrtag";

      // var channeltag = document.createElement('span');
      // channeltag.textContent = options.channelName;
      // channeltag.className += " channel-tag";
      // channeltag.style.background = options.color;

      if (typeof (G_vmlCanvasManager) !== 'undefined') {
        G_vmlCanvasManager.initElement(canvas);
      }

      var ctx = canvas.getContext('2d');
      canvas.width = canvas.height = options.size;

      el.appendChild(span);
      // span.appendChild(btag);
      // span.appendChild(adr);
      el.appendChild(canvas);
      // el.appendChild(channeltag);

      ctx.translate(options.size / 2, options.size / 2); // change center
      ctx.rotate((-1 / 2 + options.rotate / 180) * Math.PI); // rotate -90 deg

      //imd = ctx.getImageData(0, 0, 240, 240);
      var radius = (options.size - options.lineWidth) / 2;

      var drawCircle = function (color, lineWidth, percent) {
        percent = Math.min(Math.max(0, percent || 1), 1);
        ctx.beginPath();
        ctx.arc(0, 0, radius, 0, Math.PI * 2 * percent, false);
        ctx.strokeStyle = color;
        ctx.lineCap = 'square'; // butt, round or square
        ctx.lineWidth = lineWidth;
        ctx.stroke();
      };

      drawCircle('#f2f3f7', options.lineWidth, 100 / 100);
      drawCircle(options.color, options.lineWidth, options.percent / 100);
    };

    // init();

  };
})();
