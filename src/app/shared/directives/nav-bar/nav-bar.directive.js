(function () {
  'use strict';

  angular
    .module('axier.shared.directives')
    .directive('navigationBar', navigationBar);

  /** @ngInject */
  function navigationBar(profileProvider) {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'app/shared/directives/nav-bar/nav-bar.html',
      link: function (scope) {

        scope.profile = {};

        var init = function () {
          angular.copy(profileProvider.profile, scope.profile);
          scope.profile.name = 'Iestyn Daly Jones';
          scope.profile.picture = 'https://lh3.googleusercontent.com/-CqXdJiUnc6g/XDNpFSv7skI/AAAAAAAAe7E/YmX5wDCMGXk3MA7NzAModo8oZ_unokRNQCEwYBhgL/w140-h140-p/38542120_262192474388762_7698765007344893952_n%255B1%255D.jpg';
        }

        init();
      }
    };
  }
})();
