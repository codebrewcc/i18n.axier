(function () {
  'use strict';

  angular
    .module('axier.shared.directives')
    .directive('userProfile', userProfile);

  /** @ngInject */
  function userProfile(profileProvider) {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'app/shared/directives/profile/profile.html',
      link: function (scope) {

        scope.profile = {};

        var init = function () {
          angular.copy(profileProvider.profile, scope.profile);
        }

        init();
      }
    };
  }
})();
