(function () {
  'use strict';

  angular
    .module('axier.shared.directives')
    .directive('axierIcon', axierIcon)

  /** @ngInject */
  function axierIcon(enums) {
    return {
      restrict: 'E',
      replace: true,
      link: function (_scope, ele, attrs) {
        ele[0].innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="' + attrs.iconSize + '" height="' + attrs.iconSize + '" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">' + enums.iconPaths[attrs.iconPath] + '</svg>';
      },
      template: '<span class="axier__icon"></span>',
    };
  }
})();
