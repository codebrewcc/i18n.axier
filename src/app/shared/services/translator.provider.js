(function () {
  'use strict';

  angular
    .module('axier')
    .provider('translatorProvider', translatorProvider);

  /** @ngInject */
  function translatorProvider() {

    var $cookies;

    angular.injector(['ngCookies']).invoke([
      '$cookies', function (_$cookies) {
        $cookies = _$cookies;
      }
    ]);

    this.$get = ['$injector', function ($injector) {
      var $rootScope = $injector.get('$rootScope');

      var service = {
        setMenu: setMenu,
        keys: {},
      };

      return service;

      function setMenu(jsonConfig) {
        this.keys = jsonConfig;
      }

    }];
  }
})();
