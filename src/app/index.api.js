(function () {
  'use strict';

  angular
    .module('axier')
    .constant('APP_URI_CONFIG', {
      "apiBaseUrl": 'https://api.axier.io',
      "appBaseUrl": 'http://localhost:8712/'
    });

})();
