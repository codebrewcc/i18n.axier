(function () {
  'use strict';

  angular
    .module('axier')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider, $locationProvider, $ocLazyLoadProvider) {

    $locationProvider.html5Mode(true);

    $urlRouterProvider.otherwise('/');

    $stateProvider
      .state('app', {
        abstract: true,
        template: '<ui-view></ui-view>',
        url: '',
        resolve: {
          sessionInfo: function (authService) {
            return authService.checkSession().then(function (res) {
              authService.setSession(res);
              return res;
            })
          }
        }
      })
      .state('callback', {
        url: '/callback',
        controller: function ($state, $timeout, angularAuth0, authService) {
          angularAuth0.parseHash(function (err, authResult) {
            // alert(JSON.stringify(authResult));
            if (authResult && authResult.accessToken && authResult.idToken) {
              debugger;
              authService.setSession(authResult)
              $state.go('app.base.dock');
            } else if (err) {
              $timeout(function () {
                $state.go('app.base.dock');
              });
              alert('Error: ' + err.error + '. Check the console for further details.');
            }
          });
        },
      })
      .state('app.base', {
        abstract: true,
        url: '',
        templateUrl: 'app/shared/tpls/app.html',
        // template: '<ui-view></ui-view>',
        controller: 'AppController as vm',
        resolve: {
          appTpls: ['$ocLazyLoad', function ($ocLazyLoad) {
            // document.getElementsByClassName("page-loading-overlay")[0].classList.add("loaded");
            return $ocLazyLoad.load('axier.app'); // Resolve promise and load before view
          }],
          UserProfile: ['authService', 'angularAuth0', 'profileProvider', '$log', function (authService, angularAuth0, profileProvider, $log) {
            return angularAuth0.client.userInfo(localStorage.getItem('access_token'), function (err, profile) {
              if (profile) {
                profileProvider.setProfile(profile);
              } else {
                $log.error(err);
                // authService.logOut();
              }
            });
          }]
        }
      })
      .state('app.base.dock', {
        url: '/',
        templateUrl: 'app/shared/tpls/home.html',
        controller: function ($state) {
          $state.go('app.base.translator.section', { 'id': 1, 'section': 'ERRORS' });
        },
        // resolve: {
        //   app: function ($state) {
        //     $state.go('app.base.translator.section.subsection');
        //   }
        // }
      });
  }
})();
