(function () {
  'use strict';

  angular
    .module('axier')
    .constant('APP_URI_CONFIG', {
      "apiBaseUrl": 'https://api.axier.io',
      "appBaseUrl": 'https://i18n.axier.io/'
    });

})();
