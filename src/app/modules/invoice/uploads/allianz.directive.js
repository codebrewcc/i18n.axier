(function () {
  'use strict';

  angular
    .module('axier.app.invoice')
    .directive('allianzInvoice', allianzInvoice);

  /** @ngInject */
  function allianzInvoice() {
    return {
      restrict: 'A',
      scope: {
        allianzInvoice: "="
      },
      link: function (scope, element) {

        //var csv is the CSV file with headers
        function csvJSON(csv) {

          var lines = csv.split("\n");

          var result = [];

          var headers = lines[0].split(',');

          for (var i = 1; i < lines.length; i++) {

            var obj = {};
            var tempLine = lines[i].replaceAt(8, " ");
            var currentline = tempLine.split(',');

            for (var j = 0; j < headers.length; j++) {
              switch (headers[j]) {
                case 'Date Worked':
                  var extractedDate = currentline[j] + currentline[''];
                  try {
                    obj['date_worked'] =
                      typeof extractedDate === 'string' ?
                      moment(extractedDate.substr(1, 12)).format('YYYY-MM-DD') : '';
                    // console.info(moment(extractedDate.substr(1, 12)).format('YYYY-MM-DD'));
                  } catch (err) {
                    // console.error('check error');
                    // console.info(err);
                  }
                  break;
                default:
                  obj[headers[j].toLowerCase().replace(/"/g, '').replace(/ /g, '_')] =
                    currentline[j];
                  break;
              }

            }

            result.push(obj);

          }
          result.shift();
          result.pop(result.length);

          var resultSet = {
            charges: {},
            dateFrom: result[0].date_worked,
            dateTo: result[0].date_worked,
            logs: [],
            calendar: [],
            calendarTimesheet: {}
          };

          for (var i = 0; i < result.length; i++) {
            if (result[i].date_worked < resultSet.dateFrom) {
              resultSet.dateFrom = result[i].date_worked;
            }

            if (result[i].date_worked > resultSet.dateTo) {
              resultSet.dateTo = result[i].date_worked;
            }

            if (!resultSet.charges[result[i].id] && result[i].id) {
              resultSet.charges[result[i].id] = {
                id: result[i].id,
                project: result[i].project_plan,
                title: result[i].title,
                hours: parseFloat(result[i].hours)
              }
            } else if (result[i].id) {
              resultSet.charges[result[i].id].hours += parseFloat(result[i].hours);
            }
            resultSet.logs.push(result[i]);
          }

          // update resultSet with days derived from hours
          for (var k in resultSet.charges) {
            if (resultSet.charges.hasOwnProperty(k)) {
              resultSet.charges[k].days = parseFloat(resultSet.charges[k].hours / 8);
            }
          }

          for (var i = 0; i < resultSet.logs.length; i++) {
            var dateWorked = resultSet.logs[i].date_worked;
            if (!resultSet.calendarTimesheet[dateWorked]) {
              resultSet.calendarTimesheet[dateWorked] = parseFloat(resultSet.logs[i].hours);
            } else {
              resultSet.calendarTimesheet[dateWorked] += parseFloat(resultSet.logs[i].hours);
            }
          }


          var month = moment(resultSet.dateFrom).month();
          var year = moment(resultSet.dateFrom).year();
          var date = moment.utc(year + '-' + (month + 1) + '-01Z').startOf('week');
          var calendar = [];
          for (var i = 0; i < 5; i++) {
            if (i) {
              date = moment(date).add(1, 'weeks');
            }

            calendar.push({
              week: moment(date).week(),
              days: Array(7).fill(0).map((n, i) => moment(date).startOf('week').clone().add(n + i, 'day').format('YYYY-MM-DD'))
            })
          }
          console.info(calendar);

          resultSet['calendar'] = calendar;

          return resultSet; //JavaScript object
          // return JSON.stringify(result); //JSON
        }

        element.on('change', function (changeEvent) {
          var files = changeEvent.target.files;
          if (files.length) {
            var r = new FileReader();
            r.onload = function (e) {
              var contents = e.target.result;
              console.info(e);
              scope.$apply(function () {
                scope.allianzInvoice = csvJSON(contents);
              });
            };

            r.readAsText(files[0]);
          }
        });
      }
    }
  }
})();
