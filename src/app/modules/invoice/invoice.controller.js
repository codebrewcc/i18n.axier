(function () {
  'use strict';

  angular
    .module('axier.app.invoice')
    .controller('InvoiceController', InvoiceController);

  /** @ngInject */
  function InvoiceController($scope, $state) {
    var vm = this;

    vm.dom = {
      keys: {},
      content: {},
      fullscreen: false,
      view: 1
    };

    vm.invoice = {
      title: 'test',
      from: {
        address: 'ttt',
        website: 'ttttt',
        phone: '234',
        email: 'test@test.com'
      },
      days: 21,
      rate: 350
    }

    vm.fns = {
      getTotalDays: function (charges) {
        var days = 0;
        for (var key in charges) {
          days += charges[key].days
        }
        return days;
      },
      getSubtotal: function (days, rate) {
        return days * rate;
      },
      getVAT: function (days, rate, vat) {
        return days * rate * vat;
      },
      getTotal: function (days, rate, vat) {
        return (days * rate) + (days * rate * vat);
      },
      isDayDisabled: function (month, date) {
        return !(new Date(date).getMonth() === (month - 1));
      }
    }

    $scope.$watchCollection(function () {
      return $state;
    }, function (nv) {
      console.info(nv);
    });

  };
})();
