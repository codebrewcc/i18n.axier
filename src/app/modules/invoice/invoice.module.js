﻿(function () {
  'use strict';

  angular
    .module('axier.app.invoice', [

    ])
    .config(config);

  /** @ngInject */
  function config($stateProvider) {
    $stateProvider
      .state('app.base.invoice', {
        url: '/invoice?print',
        templateUrl: 'app/modules/invoice/invoice.html',
        controller: 'InvoiceController as vm'
      });

  };
})();
