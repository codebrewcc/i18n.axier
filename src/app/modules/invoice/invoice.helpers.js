(function () {
  'use strict';

  angular
    .module('axier.app.invoice')
    .factory('invoiceHelpers', invoiceHelpers);

  function invoiceHelpers() {
    var service = {};

    service.getIndexInArray = function (arr, key, value) {
      for (var i = 0; i < arr.length; i++) {
        if (arr[i][key] == value) {
          return i;
        }
      }
    }

    return service;
  };
})();
