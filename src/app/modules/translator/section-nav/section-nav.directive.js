(function () {
  'use strict';

  angular
    .module('axier.app.translator')
    .directive('sectionNav', sectionNav);

  /** @ngInject */
  function sectionNav($state) {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'app/modules/translator/section-nav/section-nav.html',
      scope: {
        "keys": "="
      },
      // link: function (scope, ele, attr) {
      //   scope.$state = $state;

      //   // scope.$on('keys_updated', function (event, args) {
      //   //   alert('test');
      //   //   console.error(args);
      //   // });
      // },
      controllerAs: 'vm',
      controller: function ($scope, $rootScope, $timeout, translatorProvider, Popeye) {
        var vm = this;

        vm.dom = {
          selected: {
            section: null
          }
        };

        // $scope.dom = {
        //   sections: {}
        // }


        // $rootScope.$on('i18n:keys_updated', function (_event, args) {
        //   angular.copy(args.keys, $scope.dom.sections);
        // });

        $scope.$watch(function () {
          return $state.params.section;
        }, function (nv) {
          if (angular.isDefined(nv)) {
            vm.dom.selected.section = nv;
          }
        });

        $scope.fns = {
          getFirstKey: function (sub_sections) {
            return Object.keys(sub_sections)[0]
          },
          promptAddSection: function () {
            var modal = Popeye.openModal({
              templateUrl: 'app/modules/translator/section-nav/add.tpl.html',
              controller: function ($scope) {

                $scope.dom = {
                  "key": "NOTIFICATIONS",
                  "label": "Notifications",
                  "icon": "circle"
                }

                $scope.fns = {
                  postSection: function () {
                    modal.close($scope.dom);
                  }
                }

              }
            });

            modal.closed.then(function (keySet) {
              $scope.keys[keySet.key] = {
                "label": keySet.label,
                "icon": keySet.icon,
                "subs": [],
                "values": []
              }
            });
          }
        };

        // vm.init = function () {

        // }

        // vm.init();
      }
    };
  }
})();
