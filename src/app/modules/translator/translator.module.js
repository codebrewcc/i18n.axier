﻿(function () {
  'use strict';

  angular
    .module('axier.app.translator', [

    ])
    .config(config);

  /** @ngInject */
  function config($stateProvider) {
    $stateProvider
      .state('app.base.translator', {
        url: '/:id',
        abstract: true,
        template: '<div class="flex" ui-view></div>',
        // templateUrl: 'app/shared/tpls/app.html',
        resolve: {
          Config: function ($http, APP_URI_CONFIG) {
            return $http.get(APP_URI_CONFIG.apiBaseUrl + '/v1/i18n/1').then(function (res) {
              console.info(res);
              // $rootScope.$broadcast('i18n:keys_updated', { keys: res.data });
              return res.data.data;
            });
          }
        },
        controller: 'TranslatorController as vm'
      })
      .state('app.base.translator.section', {
        url: '/:section',
        templateUrl: 'app/modules/translator/translator.html',
        // controller: 'TranslatorController as vm',
        // resolve: {
        //   Users: function (TranslatorService) {
        //     return TranslatorService.list().then(function (res) {
        //       return res.data;
        //     });
        //   }
        // }
      });

  };
})();
