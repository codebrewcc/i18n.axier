(function () {
  'use strict';

  angular
    .module('axier.app.translator')
    .controller('TranslatorController', TranslatorController);

  /** @ngInject */
  function TranslatorController(Config, $timeout, $scope, $http, $state, $rootScope, translatorProvider, TranslatorService) {
    console.info(Config);

    var init = true;
    var vm = this;

    vm.dom = {
      keys: {},
      langs: [
        { "lang": "en-gb", "label": "English" },
        { "lang": "es", "label": "Español" },
        { "lang": "de-de", "label": "Deutsch" }
      ],
      selected: {
        section: null
      },
      lang_keys: {
        'en-gb': {},
        'de-de': {},
        'cr': {}
      }
    };

    vm.fns = {
      // updateValue: function (key, value) {
      //   console.info(value);
      //   vm.dom.keys[vm.dom.selected.section].sub_sections[vm.dom.selected.sub].values[key] = value
      // },
      addKey: function () {
        vm.dom.keys[vm.dom.selected.section].values.push({ key: "NEW", key_description: "description", index: vm.dom.keys[vm.dom.selected.section].values.length });

      },
      copyKey: function (section, sub, key) {
        var copyEle = document.getElementById('copyEle');
        copyEle.value = [section, sub, key].join('.')
        copyEle.select();
        document.execCommand("copy");
      }
    }

    translatorProvider.setMenu(Config.i18n_keys);

    var init = function () {
      angular.copy(Config.i18n_keys, vm.dom.keys);
      // angular.copy(Config.langs, vm.dom.langs);
      vm.dom.selected.section = $state.params.section;
      $scope.$watch('vm.dom.keys', function (nv) {
        console.info(nv);
        if (!init) {
          TranslatorService.updateKeys(1, nv).then(function (res) {
            console.info(res);
          })
        } else {
          init = false;
        }

      }, true);
      // vm.dom.selected.sub = $state.params.sub;
    }

    init();

    $scope.$watchCollection(function () {
      return $state.params;
    }, function (nv) {
      if (angular.isDefined(nv.section)) {
        vm.dom.selected.section = nv.section;
      }
      // if (angular.isDefined(nv.sub)) {
      //   vm.dom.selected.sub = nv.sub;
      // }
    });


  };
})();
